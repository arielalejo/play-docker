## 
## install python 3.7
## 
FROM amazonlinux:2

RUN yum upgrade -y
RUN yum install -y python3.7 make gcc curl gcc-c++


# python
# RUN cd /usr/src
# RUN wget https://www.python.org/ftp/python/3.7.9/Python-3.7.9.tgz
# RUN tar xzf Python-3.7.9.tgz
# RUN ./configure --enable-optimizations
# RUN make altinstall


WORKDIR /usr/src/app