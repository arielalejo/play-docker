FROM node:16-alpine3.17
VOLUME /apps/jenkins
RUN apk update
RUN apk add git
RUN apk add github-cli
RUN apk add openssl
